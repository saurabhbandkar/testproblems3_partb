function limitFunctionCallCount(callBackFunction, counterVariable) {

    return function callingCbFunction(...args) {
        if (counterVariable > 0) {
            counterVariable -= 1;
            return callBackFunction(...args);
        }
        else {
            return null;
        };
    };
};

