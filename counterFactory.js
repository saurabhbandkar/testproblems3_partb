function counterFactory() {

  let counterVariable = 0;

  function incrementCounter() {

    counterVariable += 1;

    console.log(counterVariable);
  };
  function decrementCounter() {

    counterVariable -= 1;

    console.log(counterVariable);
  };
  return new Object({
    'increment': function () {
      incrementCounter()
    },
    'decrement': function () {
      decrementCounter()
    }
  });
};
