function limitFunctionCallCount(callBackFunction, counterVariable) {
    
    return function callingCbFunction(...args) {
        if (counterVariable > 0) {
            counterVariable -= 1;
            return callBackFunction(...args);
        }
        else {
            return null;
        };
    };
};


function displayMessage() {
    return 'Testing Success';
}

function doSquares(x) {
    return x ** 2;
}

const testWithoutArgs = limitFunctionCallCount(displayMessage, 3);
for (let iterator = 1; iterator < 6; iterator++) {
    console.log(testWithoutArgs());
};



const testWithArgs = limitFunctionCallCount(doSquares, 4);
for (let iterator = 1; iterator < 6; iterator++) {
    console.log(testWithArgs(iterator));
};
