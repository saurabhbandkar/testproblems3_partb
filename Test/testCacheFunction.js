function cacheFunction(callBackFunction) {

    const previousArgs = new Object();

    return function callingCbFunction(...args) {
        
        let currentArgs = Array.from(arguments);

        if (Object.keys(previousArgs).includes(String(currentArgs))) {
            console.log('This is not a function call');
            return previousArgs[currentArgs];
        }
        else {
            
            const result = callBackFunction(...args);

            previousArgs[currentArgs] = result;
            console.log('This is a function call');
            return result;

        };

    };

};

function doSquares(x) {
    return x ** 2;
};

const test = cacheFunction(doSquares);
console.log(test(1));
console.log(test(2));
console.log(test(3));
console.log(test(4));
console.log(test(1));
console.log(test(2));
console.log(test(3));
console.log(test(4));