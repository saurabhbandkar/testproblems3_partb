function counterFactory() {

  let counterVariable = 0;

  function incrementCounter() {

    counterVariable += 1;
    
    console.log(counterVariable);
  };
  function decrementCounter() {

    counterVariable -= 1;

    console.log(counterVariable);
  };
  return new Object({
    'increment': function () {
      incrementCounter()
    },
    'decrement': function () {
      decrementCounter()
    }
  });
};


const testCode = counterFactory();

for (let iterator = 1; iterator <= 10; iterator++) {
  if (iterator % 3 == 0) {
    testCode.decrement();
  }
  else {
    testCode.increment();
  }
};