function cacheFunction(callBackFunction) {

    const previousArgs = new Object();

    return function callingCbFunction(...args) {

        let currentArgs = Array.from(arguments);

        if (Object.keys(previousArgs).includes(String(currentArgs))) {
            console.log('This is not a function call');
            return previousArgs[currentArgs];
        }
        else {

            const result = callBackFunction(...args);
            
            previousArgs[currentArgs] = result;
            console.log('This is a function call');
            return result;

        };

    };

};